package com.example.demo;

import com.example.demo.service.CacheService;
import com.example.demo.service.ControlledCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import java.util.UUID;

import static java.util.Objects.isNull;

@SpringBootApplication
@EnableCaching
public class RedisDemoApplication implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(RedisDemoApplication.class);

	@Autowired
	CacheService cacheService;

	@Autowired
	ControlledCacheService controlledCacheService;

	public static void main(String[] args) {
		SpringApplication.run(RedisDemoApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		String firstString = cacheService.cacheThis("param1", UUID.randomUUID().toString());
		logger.info("First: {}", firstString);
		String secondString = cacheService.cacheThis("param1", UUID.randomUUID().toString());
		logger.info("Second: {}", secondString);
		String thirdString = cacheService.cacheThis("AnotherParam", UUID.randomUUID().toString());
		logger.info("Third: {}", thirdString);
		String fourthString = cacheService.cacheThis("AnotherParam", UUID.randomUUID().toString());
		logger.info("Fourth: {}", fourthString);

		logger.info("Starting controlled cache: -----------");
		String controlledFirst = getFromControlledCache("first");
		logger.info("Controlled First: {}", controlledFirst);
		String controlledSecond = getFromControlledCache("second");
		logger.info("Controlled Second: {}", controlledSecond);

		getFromControlledCache("first");
		getFromControlledCache("second");
		getFromControlledCache("third");
	}

	private String getFromControlledCache(String param) {
		String fromCache = controlledCacheService.getFromCache(param);

		if(isNull(fromCache)) {
			logger.info("Empty cache. Generating...");
			String newValue = controlledCacheService.populateCache(param, UUID.randomUUID().toString());
			logger.info("Cache populated with: {}", newValue);
			return newValue;
		}
		logger.info("Returning from cache: {}", fromCache);
		return fromCache;
	}
}
